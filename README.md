# Go pipeline workshop

## [Step-0](./step-0/README-md): Initial codebase and explanation

In this step we will cover the initial codebase to CI/CD.

## [Step-1](./step-1/README-md): Let's make testable

We require to think in testing before go to plumbing, need to be efective and testable

## [Step-2](./step-2/README-md): Do the plumbing

Introduction to GitLabCI/CD pipelines

## [Step-3](./step-3/README-md): To infinity and beyond!

The last one is to check this plumbing on the GitLab and if we have time deploy to heroku
