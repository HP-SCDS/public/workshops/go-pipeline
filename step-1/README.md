# Step-1: Let's make testable

We need a litle refactor in order to be testable.

1. Refactor main.go in order to make it more testable (split into funcions or even better into a controller, to make swagger documentation better)(*)
1. Create testing files/functions
1. Let's run the tests (`make test`)
1. Let's test the coverge (`make coverage`)
1. Let's view the HTML report of coverage (`make coverhtml`)
1. Let's make 100% coverage

---
### Notes (*)
(*) Check the docs for accomplish this on [SWAG](https://github.com/swaggo/swag#api-operation)
