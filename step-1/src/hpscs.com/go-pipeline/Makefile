PROJECT_NAME := "go-pipeline"
PKG := "$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/...)
GO_LINT := $(shell go list -f {{.Target}} golang.org/x/lint/golint)
BUILD_FOLDER := "build"
GO_SWAG := $(shell go list -f {{.Target}} github.com/swaggo/swag/cmd/swag)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

run:
	@go run main.go

swag:
	@${GO_SWAG} init

lint: ## Lint the files
	@${GO_LINT} -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}

coverage:
	@go test -cover ${PKG_LIST}

coverhtml: build_folder
	@go test ${PKG_LIST} -coverprofile=${BUILD_FOLDER}/cover.out && go tool cover -html=${BUILD_FOLDER}/cover.out -o ${BUILD_FOLDER}/coverage.html

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

dep: ## Get the dependencies
	@go get -v -d ./...

build: dep build_folder ## Build the binary file
	@go build -o ${BUILD_FOLDER} -i -v $(PKG)

clean: ## Remove previous build
	@rm -rf ${BUILD_FOLDER}

build_folder:
	@mkdir -p ${BUILD_FOLDER}

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
