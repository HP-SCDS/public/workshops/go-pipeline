package app

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHello(t *testing.T) {

	assert.Equal(t, ExampleHello(), "hello")
}

func TestHealthCheck(t *testing.T) {

	assert.Equal(t, HealthCheck(), "Ok!")
}

func TestExampleSalutations(t *testing.T) {
	assert.Equal(t, ExampleSalutations(), "hello, and \ngoodbye")
}
